define(
	'ESP.OrderWizard.Module.Memo'
,	[	
		'Wizard.Module'
	,	'SC.Configuration'
	,	'esp.order_wizard_memo.tpl'
	,	'jQuery'
	]
,	function (
		WizardModule
	,	Configuration
	,	esp_order_wizard_memo_tpl
	,	jQuery
	)
{
	'use strict';
// console.log({'garry':'testingmemo'});
	return WizardModule.extend({

		template: esp_order_wizard_memo_tpl

	,	className: 'OrderWizard.Module.Memo'

	// ,	isActive: function ()
	// 	{
	// 		return Configuration.get('siteSettings.checkout.showmemofield', 'T') === 'T';
	// 	}

	// ,	submit: function ()
	// 	{

	// 		var purchase_order_number = this.$('[name=purchase-order-number]').val() || '';

	// 		this.wizard.model.set('purchasenumber', purchase_order_number);

	// 		return jQuery.Deferred().resolve();
	// 	}


	// ,	getContext: function ()
	// 	{		
	// 		return {

	// 			purchaseNumber: this.wizard.model.get('purchasenumber')
	// 		};
	// 	}
	});
});
