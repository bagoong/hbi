function getFilters () {
  var cats = document.getElementsByClassName('categoryLink')
  cats = Array.from(cats)
  var filterLinks = cats.filter(x => (x.attributes['x_cat_type'].value === 'FILTER'))
  return filterLinks.map(f => {
    return {
      link: f.href,
      name: f.innerText
    }
  })
}
function displayFilters (filters) {
  filters.map(filter => {
    document.getElementById('sidebar').append(
      `<p><a href="${filter.link}">${filter.name}</a></p>`)
  }
  )
}
