function showHideHomepage () {
    var itemList = $('.item-blocks')
      .children('table')
      .find('tr')
    if ($(itemList[0]).children() && $(itemList[0]).children().length === 2) {
      $(
        'btn.btn-default.btn-tab[data-carousel="carousel-item-second"]'
      ).onClick(function () {
        var itemList1 = $(itemList)[0]
        var itemList2 = $(itemList)[1]
        $(itemList1).hide()
        $('#best-seller').toggleClass('active')
        $('#new-arrivals').toggleClass('active')
        $(itemList2).show()
      })
      $('btn.btn-default.btn-tab[data-carousel="carousel-item-first"]').onClick(
        function () {
          var itemList1 = $(itemList)[0]
          var itemList2 = $(itemList)[1]
          $(itemList2).hide()
          $('#best-seller').toggleClass('active')
          $('#new-arrivals').toggleClass('active')
          $(itemList1).show()
        }
      )
    } else {
      $('#best-seller').hide()
    }
  }
  showHideHomepage()
